const Koa = require('koa'),
      Router = require('koa-router'),
      router = new Router(),
      serve = require('koa-static'),
      views = require('koa-views');
const logger = require('koa-logger');
const pug = require('koa-pug');
const app = new Koa();

// logger

app.use(logger());

// x-response-time

app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

app.use(serve(__dirname + '/build'));
app.use(views(__dirname + '/views', {extension: 'pug'}));
console.log('aatest');
// response

router.get('/', async (ctx) => {
  await ctx.render('index', {title: 'Авторизация'});
})

app.use(router.routes());

app.listen(3000);